(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgVAPQgGgGgBgJQgBgLAIgFQAGgEAKABQAFABAJAGIAUANIgUAMQgJAGgFABIgEAAQgHAAgFgFg");
	this.shape.setTransform(183.1523,86.8271,3.3989,3.3989);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgNAJQgHgIgBgEQAAgJAHgHQAHgGAIACQAGABAGAIQAGAHACAHQABACgHAHIgPAPIgNgPg");
	this.shape_1.setTransform(141.1267,88.4875,3.3989,3.3989);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ABSAWQgYgRghACQgmAIgTACQgYACgdgEQgZgDAEgTQAEgQAYACQAdACAYgBQAOAAAcgEQA0gEAnAgQgDAOgHAFQgEACgEAAQgEAAgEgDg");
	this.shape_2.setTransform(120.2842,159.5725,3.3989,3.3989);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgVAPQgGgGgBgJQgBgLAIgFQAGgEAKABQAFABAJAGIAUANIgUAMQgJAGgFABIgEAAQgHAAgFgFg");
	this.shape_3.setTransform(183.1523,86.8271,3.3989,3.3989);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgNAJQgHgIgBgEQAAgJAHgHQAHgGAIACQAGABAGAIQAGAHACAHQABACgHAHIgPAPIgNgPg");
	this.shape_4.setTransform(141.1267,88.4875,3.3989,3.3989);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("ABSAWQgYgRghACQgmAIgTACQgYACgdgEQgZgDAEgTQAEgQAYACQAdACAYgBQAOAAAcgEQA0gEAnAgQgDAOgHAFQgEACgEAAQgEAAgEgDg");
	this.shape_5.setTransform(120.2842,159.5725,3.3989,3.3989);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ag4FDIgDgHQhIgOgsgLQg/gRgxgUQg7gZgggsQgfgrgHhCQgEgeANglQAQgnAFgWQAKgkAggXQAQgMAwgWQAWgKAIgIQAMgMgDgWQgBgIAIgMQAIgLAJgFQA8gkA1gWQB7g1B/AnQA3ARAeAcQAnAiAGA3QACAVAUAOQAIAGAeAMQAoARAVAhQAwBKhAA4IgPAPQgGAIAAALQgFBohoA2IgjATIAMAIQgQAMgYAAQgLgDgEABQgpAOg4AGQgYAChJADIgOAEIABADgAk1hjQggAVgEAmQAAAAAAABQAAAAAAABQAAAAAAABQgBAAAAABQgWAlgFA2QgFAxAUAqQAVArArAVQCkBOChgFQCJgDBjhJQAqgeAOgsQAQgvgYgqQghg5gjgkQgsgsg2gUQg4gVhagSQhigThWASQgMACgEADQgIAEAAALQgDAXATAIIAKADQAFACADACIAVASQALALAGAJQATAjgFAdQgFAegfAfQgbAcggAHQgiAHgjgQQgigPgMgfQgNgfAPgeQAMgaAMgQQAQgUAWgKQAOgHAEgJQAGgLgFgQQgsARgTAMgAjvg8QgPAIgNAQIgWAdQgRAYASAbQASAaAaABQAeAAAdgYQAfgbgDgeQAAgkgZgPQgLgHgOAAQgPAAgRAIgAEMhlQgMAFgHAOIAcAcQAQAQAKANQAIAJAnA2QAagcADgVQAEgXgVgUQgNgNgWgMQgNgIgagMQgGgDgGAAIgIABgAAFiwQBJAMAdAIQA4AOAnAaQAngcgBgcQgBgXgeggQgggkhDgPQgYgFgfABIg3ADQgeACgjALIg9AXQgPAGgRAMIgeAWQgEADgEAIIgJARQA1gLA2AAQAzAAA0AKg");
	this.shape_6.setTransform(141.9712,109.6577,3.3989,3.3989);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,284,219.3), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgTADQgEgDgFgGQgEgGABgEQABgEAHgDQAGgEAFABQARABANAPQAMAPACAXIgzgZg");
	this.shape.setTransform(134.7896,64.169,3.3986,3.3986);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAPAaQgIgRgFgHQgIgKgQgCQgEgBgFgHQgFgIABgEQAAgEAHgEQAGgEAFAAQARABAKAMQAaAbAAAjQABACgEACIgHAGg");
	this.shape_1.setTransform(168.8766,100.2801,3.3986,3.3986);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAZAyQgEgBgVgMIgDACIgXgZQgMgOgGgMQgKgTAJgKQAIgLAWACQBEAHgDBQQAAAOgPAAIgKgBgAgDgLIAMASIAJgEIgRgSg");
	this.shape_2.setTransform(60.7608,115.3243,3.3986,3.3986);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("ABNEmQgRgFgJACIiPgtIAAAFIgMgIQgHgFgFAEIAAgHQgwgSg4gtQg9g2gggaQg2gsgJhUQgDgcAIgiIATg7QABgGANgKQBnhQCXAaQBxAUBmAtQAPAGArAMIgihCIgfg+QgLgXASgFQAKgEAKAGQAKAFAFAMIA/ChQABAEAGAJQA1AJA1AqQAhAZADAaQADAZgdASIgcAUIAPAmQAZBAghAxQgzBKhzALIgIABQghgFgUAMIgbgIgAjNjFQg6AKgoAlQgmAkgBA+QAAA/AnAqQBjBrBkAsQBMAiA7AOQBKARBBgIQBbgKAqg9IAHgOQAFgJAAgHQAAgdgGgkQgFgZgegMQgGgCgHgKQgGgJABgDQAGgKgEgMIgMgUIgkhIQgGgJgFgDIgdgRQgRgKgMgFIhzgpQhCgYgygGQgmgFgfAAQgZAAgVADgAE0AZQAigTABgPQABgRgggWIgugag");
	this.shape_3.setTransform(128.451,102.7771,3.3986,3.3986);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,257,205.6), null);


// stage content:
(lib.Music_animation_600x600 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(412.3,272.05,1,1,0,0,0,142,109.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:422.05,y:279.85},9).to({x:412.3,y:272.05},9).wait(2));

	// Layer_6
	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(171,248,1,1,0,0,0,128.4,102.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:257.75},9).to({y:248},9).wait(2));

	// Armature_11
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.067)").s().p("AgDAAIhegzQBUghAPBPIBgA/QgUAEgQAAQhBAAAAg+g");
	this.shape.setTransform(509.425,436.7112);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ABGGgQiQiMifiBQgtgkgYgtQAAgJADgLQAPguBAgfQBagqBcgcIANgGQBDgbBAgpQhViOiMgaQhDgwhOgqIgWgKQBFAABFgnIEyDHQAnAZAkAdQAiAfACAgQAEAuhCAhIjTBkIAxAmIC1CLQBsBUgtBjQgUBXhSAUQgPADgOAAQg+AAgahDgAgzCcIBcA0QAABMBmgSIhfhAQgLg4gsAAQgTAAgZAKg");
	this.shape_1.setTransform(504.8514,415.9362);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.067)").s().p("AgDABIhggwQBTgkARBOIBjA7QgXAFgSAAQg8AAgCg6g");
	this.shape_2.setTransform(510.25,438.6752);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("ABQGrQiWiGilh6QgugjgZgrQgCgKADgLQANguA/ghQBYguBbghIANgGQBBgeA+grQhaiLiCguQhBgyhNgrIgWgLQBFABBFglIEuDPQAlAaAkAeQAhAgACAgQAGAuhAAkIjPBsIAyAkIC7CDQBvBQgoBlQgRBYhQAXQgSAFgQAAQg7AAgbhAgAg0CsIBeAxQAEBMBlgXIhjg8QgNg1gpAAQgUAAgaALg");
	this.shape_3.setTransform(505.5663,416.6588);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.067)").s().p("AgDABIhigsQBRgnAVBNIBlA3QgaAHgUAAQg4AAgDg4g");
	this.shape_4.setTransform(511.05,440.5092);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("ABZG1Qibh/iqhzQgwgggbgrQgBgKACgLQAMgvA8gkQBXgxBZgkIANgHQA/ggA9gvQheiGh6hDQg/gzhMguIgWgLQBFADBGgkIEpDXQAlAbAjAfQAgAgABAhQAHAug+AmIjKB1IAzAhIDBB8QByBLgkBmQgNBZhQAbQgUAGgRAAQg4AAgdg9gAg1C8IBgAtQAGBMBlgbIhlg4QgPgzgnAAQgVAAgbANg");
	this.shape_5.setTransform(506.2934,417.324);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.067)").s().p("AgDABIhkgnQBPgsAZBNIBnAzQgdAJgVAAQgzAAgGg2g");
	this.shape_6.setTransform(511.825,442.2606);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("ABiG/Qihh5ithrQgygggdgpQgBgJABgLQAJgvA8goQBUg0BXgoIANgHQA+gjA7gxQhkiChvhXQg/g1hKgwIgVgMQBFAGBHgjIEjDeQAkAcAiAgQAgAiAAAgQAJAug9AoIjFB+IA1AfIDGBzQB1BGggBoQgJBZhPAeQgWAIgTAAQg1AAgeg5gAg3DMIBjAoQAKBMBigfIhng0QgRgxglAAQgVAAgdAQg");
	this.shape_7.setTransform(507.0466,417.9313);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.067)").s().p("AgEABIhlgjQBOguAbBLIBqAvQgfALgXAAQgwAAgIg0g");
	this.shape_8.setTransform(512.65,443.9287);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("ABrHIQimhyiyhkQgzgdgegoQgCgJABgMQAHgwA6gpQBSg4BVgrIANgIQA9gmA4gzQhqh+hkhrQg+g4hJgwIgVgNQBFAHBIggIEdDlQAjAdAiAgQAfAjgBAgQALAug7AqIjACGIA2AdIDLBrQB4BBgbBpQgGBahNAhQgZAJgVAAQgxAAgfg1gAg4DcIBjAkQANBLBigjIhpgwQgSgugjAAQgXAAgdASg");
	this.shape_9.setTransform(507.8313,418.4845);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(255,255,255,0.067)").s().p("AgEABIhmgfQBLgxAgBKIBqAqQghANgXAAQgtAAgKgxg");
	this.shape_10.setTransform(513.45,445.503);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AB1HQQiqhri3hdQgzgaghgnQgCgJABgMQAFgwA4grQBPg8BUguIAMgJQA8goA1g1Qhuh6hbiAQg8g4hIgzIgUgOQBEAJBJgeQCaB/B9BtQAjAeAhAhQAdAjgBAgQANAtg5AuIi6COIA3AaIDPBiQB6A8gXBrQgBBZhMAkQgaAMgXAAQgvAAgggygAg4DrIBmAgQAPBKBggnIhqgrQgUgsghAAQgYAAgeAUg");
	this.shape_11.setTransform(508.4068,418.9614);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(255,255,255,0.067)").s().p("AgEACIhngbQBJg1AiBJIBtAmQgjAPgZAAQgqAAgLgug");
	this.shape_12.setTransform(514.25,446.996);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("ACAHXQiuhji6hVQg1gYgigmQgDgJAAgLQADgwA3guQBMhABSgxIAMgJQA6grAzg3Qh0h1hQiVQg7g6hHg0IgUgOQBFAKBJgcIERDzQAiAfAgAiQAdAjgDAhQAPAsg3AwIi0CVIA5AYIDSBaQB9A3gSBrQACBZhKAoQgcANgYAAQgtAAghgvgAg2D6IBnAcQASBKBfgrIhtgnQgUgqghAAQgXAAgfAWg");
	this.shape_13.setTransform(508.9387,419.3821);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(255,255,255,0.067)").s().p("AgEACIhpgXQBIg3AlBHIBuAhQglASgZAAQgoAAgMgsg");
	this.shape_14.setTransform(515.075,448.366);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("ACLHeQizhci9hNQg2gVgjglQgEgJAAgLQABgwA1gxQBJhCBQg2IAMgJQA4gtAxg5Qg8g4grhYQguhQgrg5Qg6g7hFg3IgUgOQBFAMBJgbQCTCHB4BzQAhAgAfAjQAdAkgEAgQARAsg1AyIitCdIA5AWIDWBQQB/AxgOBsQAGBahIAqQgdAQgaAAQgrAAghgsgAg1EJIBoAXQAWBJBdgvIhvgiQgVgogfAAQgYAAggAZg");
	this.shape_15.setTransform(509.4693,419.7375);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(255,255,255,0.067)").s().p("AgEACIhpgSQBEg7ApBFIBuAdQglAVgaAAQglAAgOgqg");
	this.shape_16.setTransform(515.9,449.6653);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("ACVHkQi2hUjBhFQg2gUglgiQgEgIgBgMQAAgxAygyQBHhGBNg5IAMgKQA2guAvg8Qg/g1gohhQgshWgqg8Qg3g9hEg4IgUgPQBEAOBLgZIEEEAQAhAhAeAkQAbAlgEAgQATArg0A0IimCkIA6AUIDZBHQCBAsgJBsQAJBahGAtQgeASgbAAQgpAAgigpgAgzEXIBpATQAZBIBagzIhvgeQgWglgeAAQgZAAggAbg");
	this.shape_17.setTransform(510.0013,420.05);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(255,255,255,0.067)").s().p("AgEACIhqgOQBCg9AsBDIBvAYQgmAYgbAAQgjAAgPgog");
	this.shape_18.setTransform(516.675,450.8551);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("ACfHqQi5hNjEg9Qg2gRgnghQgEgJgCgLQgCgwAwg1QBEhIBLg8IALgLQA0gxAtg9QhBgzgmhpQgqhcgohAQg2g+hCg6IgTgPQBDAQBLgXID+EHQAgAhAdAkQAaAmgFAgQAVAqgxA3IigCqIA7ARIDcA+QCDAngFBtQAOBYhFAxQgfAUgdAAQgnAAgiglgAgxElIBpAPQAcBGBYg2IhwgZQgXgjgdAAQgaAAgfAdg");
	this.shape_19.setTransform(510.5861,420.2939);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("rgba(255,255,255,0.067)").s().p("AgEACIhqgSQBFg7ApBFIBvAdQgmAVgaAAQglAAgOgqg");
	this.shape_20.setTransform(515.85,449.6739);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("ACWHkQi3hUjAhFQg3gUglgiQgDgKgBgLQgBgwAygyQBIhGBNg5IALgKQA2gvAwg7Qg/g2gphgQgshVgpg9Qg4g9hEg4IgTgPQBDAOBLgZIEFEAQAgAhAeAkQAbAlgEAgQATArgzA0IinCkIA6ATIDaBIQCBAsgKBtQAKBYhHAuQgeASgbAAQgpAAghgpgAgzEXIBpATQAZBIBagyIhvgfQgWglgfAAQgYAAggAbg");
	this.shape_21.setTransform(509.9867,420.05);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("rgba(255,255,255,0.067)").s().p("AgEACIhpgXQBHg3AmBHIBuAhQgkASgaAAQgnAAgNgsg");
	this.shape_22.setTransform(515.05,448.366);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("ACLHeQiyhci+hNQg1gXgkgjQgDgJgBgMQACgwA0gwQBKhDBQg2IALgJQA5gsAwg6Qg7g4gshYQgthPgsg6Qg5g7hFg2IgUgPQBEAMBKgaQCTCHB4BzQAhAfAfAjQAcAkgDAhQAQArg1AzIitCcIA5AWIDWBRQB/AxgOBsQAGBahIAqQgeAQgZAAQgrAAghgsgAg0EIIBnAYQAXBIBcguIhugiQgWgoggAAQgYAAgeAYg");
	this.shape_23.setTransform(509.4445,419.7625);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(255,255,255,0.067)").s().p("AgEACIhngbQBJg1AiBJIBsAmQgiAPgZAAQgqAAgLgug");
	this.shape_24.setTransform(514.225,446.996);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("ACAHXQivhji6hWQg1gYghgmQgDgJAAgLQAEgwA1guQBNg/BSgyIAMgJQA6grAzg3Qhzh1hRiUQg7g7hHg0IgUgOQBFALBJgdIERDzQAiAfAgAiQAdAkgDAgQAPAtg3AvIi0CWIA4AYIDTBaQB8A3gSBrQACBZhKAoQgcAOgYAAQgsAAghgwgAg2D6IBmAbQATBKBegrIhsgnQgUgpghAAQgXAAgfAWg");
	this.shape_25.setTransform(508.9137,419.4071);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(255,255,255,0.067)").s().p("AgEABIhmgfQBLgxAfBKIBrAqQghANgXAAQgtAAgKgxg");
	this.shape_26.setTransform(513.4,445.5139);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AB1HQQirhri2hdQgzgaghgoQgCgJABgLQAFgwA4gsQBPg7BUgvIANgIQA8gpA1g1Qhuh5hbiAQg8g4hIgzIgVgOQBFAJBIgeQCaB/B9BtQAkAeAgAhQAeAjgCAgQANAtg5AuIi6CNIA3AbIDPBjQB6A8gXBqQgCBahMAkQgaALgXAAQgvAAgfgygAg4DrIBlAgQAQBKBggmIhrgsQgTgsgiAAQgXAAgeAUg");
	this.shape_27.setTransform(508.3818,418.9838);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("rgba(255,255,255,0.067)").s().p("AgEABIhlgjQBOguAcBLIBpAvQgfALgXAAQgwAAgIg0g");
	this.shape_28.setTransform(512.55,443.9309);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("ABqHHQilhyiyhlQgygcgfgpQgCgJABgLQAHgwA6gpQBSg4BWgrIANgIQA9gmA4gzQhqh+hkhrQg+g3hJgxIgVgMQBFAHBIghIEdDlQAjAeAiAgQAfAigBAgQALAug7ArIjACFIA2AdIDKBsQB4BBgcBpQgFBahOAhQgYAKgVAAQgxAAggg3gAg4DbIBkAkQAMBLBigiIhpgwQgSgugjAAQgXAAgdARg");
	this.shape_29.setTransform(507.7251,418.5073);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("rgba(255,255,255,0.067)").s().p("AgDABIhkgoQBQgqAYBMIBnAzQgcAJgVAAQg1AAgFg2g");
	this.shape_30.setTransform(511.725,442.2644);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("ABhG+Qihh5ithsQgxgfgdgpQgBgKABgLQAJgvA8gnQBUg0BYgoIANgHQA+gjA6gxQhjiChvhXQg/g1hKgvIgWgMQBFAFBHgiIEjDeQAkAcAjAfQAfAiAAAgQAJAug8ApIjGB9IA1AfIDFB0QB1BGggBoQgJBahPAdQgWAIgTAAQg0AAgfg6gAg3DLIBiApQAJBLBjgeIhmg0QgQgxgmAAQgWAAgcAPg");
	this.shape_31.setTransform(506.9561,417.9546);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("rgba(255,255,255,0.067)").s().p("AgEABIhhgsQBRgoAVBOIBlA3QgaAHgUAAQg4AAgEg4g");
	this.shape_32.setTransform(510.925,440.5008);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("ABYG1QibiAiphzQgvghgbgrQgCgJACgLQAMgvA9glQBWgvBZglIAOgHQA/ghA9gtQheiHh6hCQg/gzhMguIgWgLQBFADBGgkIEpDXQAlAbAjAfQAgAgABAhQAHAug+AmIjLB0IA0AiIDAB8QByBMglBmQgMBZhRAaQgUAGgRAAQg4AAgdg9gAg1C8IBgAtQAGBMBkgbIhkg4QgPg0gnAAQgVAAgbAOg");
	this.shape_33.setTransform(506.1855,417.3476);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("rgba(255,255,255,0.067)").s().p("AgEAAIhfgwQBTgkASBPIBiA8QgXAFgSAAQg8AAgDg8g");
	this.shape_34.setTransform(510.125,438.6547);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("ABPGrQiWiHikh6QgugjgZgsQgBgJADgMQANgtA+giQBZgtBaggIAOgHQBBgeA+grQhaiKiCguQhBgyhNgrIgWgLQBFABBFglIEuDPQAlAaAkAeQAhAgACAgQAGAuhAAkIjQBsIAyAjIC7CFQBvBQgpBlQgRBXhRAYQgRAEgQAAQg7AAgbhAgAg0CrIBeAxQADBNBlgWIhig9QgNg2gqAAQgTAAgaALg");
	this.shape_35.setTransform(505.464,416.6662);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("rgba(255,255,255,0.067)").s().p("AgDAAIhdg0QBUggAOBPIBgBAQgUADgPAAQhCAAAAg+g");
	this.shape_36.setTransform(509.3,436.7254);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("ABGGfQiQiNifiBQgtgkgXgtQAAgKADgLQAPgsBAggQBagpBcgdIAOgFQBCgcBAgoQhUiOiNgZQhCgxhOgpIgWgLQBFAABEgnIEzDHQAmAZAlAeQAiAfACAgQADAuhBAhIjUBjIAxAmIC1CMQBqBVgsBjQgUBXhTAUQgPADgNAAQg/AAgZhFgAgzCbIBdA1QgBBMBmgSIhfhBQgLg4gtAAQgSAAgZAKg");
	this.shape_37.setTransform(504.7271,415.9774);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).to({state:[{t:this.shape_5},{t:this.shape_4}]},1).to({state:[{t:this.shape_7},{t:this.shape_6}]},1).to({state:[{t:this.shape_9},{t:this.shape_8}]},1).to({state:[{t:this.shape_11},{t:this.shape_10}]},1).to({state:[{t:this.shape_13},{t:this.shape_12}]},1).to({state:[{t:this.shape_15},{t:this.shape_14}]},1).to({state:[{t:this.shape_17},{t:this.shape_16}]},1).to({state:[{t:this.shape_19},{t:this.shape_18}]},1).to({state:[{t:this.shape_21},{t:this.shape_20}]},1).to({state:[{t:this.shape_23},{t:this.shape_22}]},1).to({state:[{t:this.shape_25},{t:this.shape_24}]},1).to({state:[{t:this.shape_27},{t:this.shape_26}]},1).to({state:[{t:this.shape_29},{t:this.shape_28}]},1).to({state:[{t:this.shape_31},{t:this.shape_30}]},1).to({state:[{t:this.shape_33},{t:this.shape_32}]},1).to({state:[{t:this.shape_35},{t:this.shape_34}]},1).to({state:[{t:this.shape_37},{t:this.shape_36}]},1).to({state:[{t:this.shape_37},{t:this.shape_36}]},1).wait(1));

	// Armature_3
	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("rgba(255,255,255,0.067)").s().p("AjJAcQCHiGEMBdQh9BBh+AAQhMAAhMgYg");
	this.shape_38.setTransform(446.775,479.9836);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AjtGnQgYg9AFhVQAFg/gNg+Ig0jNIh4nLIgTgzIBKAAICVHsIA5C8QAaBZAfAdQAtApBngCQCJAICIAXQAyAKA4ASQAuAXABA/QgMBwicgJQiCAaiCANQgYADgWAAQiiAAg5iNgAh3HAQDKBADJhrQhtgmhYAAQh+AAhQBRg");
	this.shape_39.setTransform(438.55,438.033);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("rgba(255,255,255,0.067)").s().p("AjJAgQCEiJEPBWQiCBIiDAAQhGAAhIgVg");
	this.shape_40.setTransform(446.65,479.2611);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AjuGqQgYg8AFhVQAGhAgNg+IgzjNIh5nLIgTgzIBLAAICUHtIA5C8QAaBYAfAeQAtAoBigIQCKAECJAUQAyAJA4ARQAvAWADA+QgLBwibgFQiBAeiCAQQgdADgZAAQibAAg8iHgAh3HAQDMA7DGhvQhogihUAAQiFAAhRBWg");
	this.shape_41.setTransform(438.475,437.7403);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("rgba(255,255,255,0.067)").s().p("AjIAlQCAiOERBRQiFBOiJAAQhBAAhCgRg");
	this.shape_42.setTransform(446.5,478.5655);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AjuGtQgag8AIhWQAFg/gMg/IgzjMIh4nLIgTg0IBKABICUHsIA5C9QAaBYAeAeQAtAoBegPQCLABCIAQQAzAIA3AQQAwAVAFA+QgHBwicgBQiAAhiDATQghAFgdAAQiUAAg9iCgAh3HAQDOA2DEh0QhjgdhQAAQiMAAhTBbg");
	this.shape_43.setTransform(438.35,437.4896);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("rgba(255,255,255,0.067)").s().p("AjHApQB9iQESBJQiJBViQAAQg6AAg8gOg");
	this.shape_44.setTransform(446.275,477.8728);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AjuGvQgag8AHhVQAGhAgMg+IgyjNQg7jmg9jlIgTg0IBLABICTHtIA4C8QAaBZAfAdQAsApBagWQCKgCCJANQAzAGA4AOQAxAUAFA+QgEBxicADQiAAjiBAXQgkAHghAAQiPAAg+h+gAh3G/QDPAxDBh5QhdgYhLAAQiUAAhUBgg");
	this.shape_45.setTransform(438.225,437.2652);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("rgba(255,255,255,0.067)").s().p("AjGAtQB5iSEUBCQiMBciVAAQg2AAg2gMg");
	this.shape_46.setTransform(446.075,477.1992);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AjuGyQgag8AIhWQAGg/gMg/IgyjNQg7jlg9jmIgTgzIBLAAQBYEjA7DKIA4C9QAZBZAfAdQAsApBWgcQCKgGCKAKQAyAFA5ANQAxASAHA+QgCBxicAHQh+AliBAbQgoAIglAAQiJAAg/h4gAh2G/QDQAsC9h9QhWgVhHAAQidAAhTBmg");
	this.shape_47.setTransform(438.075,437.026);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("rgba(255,255,255,0.067)").s().p("AjFAxQB2iVEVA7QiPBjidAAQgvAAgwgJg");
	this.shape_48.setTransform(445.875,476.5071);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AjuG0Qgbg8AJhWQAGg/gMg/IgxjMIh3nMIgTgzIBLAAQBYEjA7DKIA3C9QAZBaAfAdQArApBSgjQCKgJCJAGQBlAJBBBWQABBwicALQh9ApiAAdQgtALgoAAQiDAAhAh0gAh2G+QDRAnC7iCQhPgQhCAAQimAAhVBrg");
	this.shape_49.setTransform(437.9003,436.8121);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("rgba(255,255,255,0.067)").s().p("AjEA2QByiaEXA2QiSBrijAAQgqAAgqgHg");
	this.shape_50.setTransform(445.6,475.804);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AjuG3Qgbg8AKhWQAGg/gLg/IgxjNQg6jmg9jlIgTg0IBLABQBXEjA7DKQAgBqAXBTQAZBaAfAdQArApBNgpQCJgNCKADQBlAGBDBVQADBwibAPQh8Ash/AgQgxANgrAAQh9AAhChvgAh1G+QDSAiC3iHQhIgOg9AAQiwAAhUBzg");
	this.shape_51.setTransform(437.7278,436.5769);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("rgba(255,255,255,0.067)").s().p("AjDA6QBuicEZAvQiWByipAAQgjAAglgFg");
	this.shape_52.setTransform(445.375,475.1177);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AjtG5Qgdg7AMhXQAHg/gMg/IgwjNQg6jmg9jlIgTg0IBLABQBXEjA7DKIA2C+QAZBYAfAeQArAqBIgwQCJgPCKgBQBlADBFBUQAGBwibASQh7Avh+AjQg1AQgtAAQh5AAhChrgAh0G9QDSAdC0iLQhAgKg4AAQi6AAhUB4g");
	this.shape_53.setTransform(437.5112,436.3624);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("rgba(255,255,255,0.067)").s().p("AjBA+QBqieEZAnQiXB6iwAAQgeAAgegDg");
	this.shape_54.setTransform(445.075,474.4539);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#000000").s().p("AjsG7Qgdg6AMhYQAHg/gLg/IgwjNQg6jmg9jlIgTg0IBLABQBYEjA6DKQAfBrAXBTQAZBZAeAeQAsApBDg1QCIgUCKgDQBkABBIBRQAJBxibAVQh6Azh9AmQg4ASgxAAQh0AAhChngAhzG9QDSAYCxiQQg4gHgxAAQjGAAhUB/g");
	this.shape_55.setTransform(437.2998,436.1552);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("rgba(255,255,255,0.067)").s().p("AjABCQBmigEbAgQiaCCi4AAQgXAAgYgCg");
	this.shape_56.setTransform(444.775,473.7555);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AjsG+Qgdg6ANhYQAHg/gLg/IgvjNQg5jmg+jmIgTgzIBLAAQBYEjA6DLIA1C+QAYBZAfAeQArAqA/g8QCHgXCKgHQBkgBBKBQQALBwiaAZQh4A1h9ApQg7AUgzAAQhvAAhEhhgAhzG8QDUATCtiTQgwgGgqAAQjSAAhVCGg");
	this.shape_57.setTransform(437.065,435.9348);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AjsG8Qgdg7AMhXQAHg/gLg/IgwjNQg6jmg9jmIgTgzIBLAAQBYEjA6DLIA2C+QAYBYAfAeQAsAqBDg2QCIgTCKgEQBlABBHBRQAJBwibAWQh6Azh9AmQg4ARgwAAQh0AAhDhlgAhzG9QDTAYCwiQQg4gIgxAAQjFAAhVCAg");
	this.shape_58.setTransform(437.2998,436.1324);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("rgba(255,255,255,0.067)").s().p("AjDA6QBvibEYAtQiWBziqAAQgjAAgkgFg");
	this.shape_59.setTransform(445.375,475.0793);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AjtG6Qgcg7ALhXQAGhAgLg+IgwjNQg6jmg9jmIgTgzIBLAAQBXEjA7DLIA2C9QAZBZAfAeQArApBIgwQCJgQCKAAQBlADBFBTQAGBwiaASQh8Awh+AjQg0APguAAQh4AAhDhpgAh0G+QDSAcC0iLQg/gLg3AAQi7AAhVB6g");
	this.shape_60.setTransform(437.5362,436.3393);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("rgba(255,255,255,0.067)").s().p("AjEA2QByiZEXA0QiTBsikAAQgoAAgqgHg");
	this.shape_61.setTransform(445.675,475.7083);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AjtG3Qgcg7ALhXQAGg/gMg/IgxjNQg6jmg9jlIgTg0IBLABQBXEjA7DKIA3C9QAZBZAfAeQAsAoBMgpQCKgNCJADQBmAFBCBVQAFBwicAPQh8Ath/AgQgxAOgrAAQh9AAhChvgAh0G+QDRAhC3iHQhHgNg8AAQixAAhUBzg");
	this.shape_62.setTransform(437.7313,436.5519);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("rgba(255,255,255,0.067)").s().p("AjFAyQB1iWEWA7QiQBkieAAQguAAgvgJg");
	this.shape_63.setTransform(445.95,476.3766);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("AjuG1Qgbg8AJhWQAHg/gNg/IgxjNIh4nLIgTg0IBLABQBYEiA7DLIA4C9QAZBYAfAeQAsAqBQglQCKgJCKAGQBlAHBBBXQABBwibAMQh+Aph/AeQguALgoAAQiCAAhBhzgAh1G+QDRAmC6iCQhOgRhCAAQinAAhUBtg");
	this.shape_64.setTransform(437.9503,436.7502);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("rgba(255,255,255,0.067)").s().p("AjGAuQB4iTEVBBQiNBdiXAAQg0AAg1gLg");
	this.shape_65.setTransform(446.2,477.0405);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#000000").s().p("AjuGyQgag8AIhVQAGhAgMg+IgyjNQg7jmg9jlIgTg0IBLABICTHtIA4C8QAZBZAfAdQAtApBVgdQCKgGCJAJQAzAFA5AMQAxATAHA+QgBBwicAIQh+AmiBAbQgqAJglAAQiHAAhAh4gAh1G/QDQArC9h/QhVgUhGAAQifAAhTBog");
	this.shape_66.setTransform(438.125,436.9785);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("rgba(255,255,255,0.067)").s().p("AjHAqQB8iRETBIQiKBXiQAAQg6AAg7gOg");
	this.shape_67.setTransform(446.425,477.6988);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#000000").s().p("AjtGwQgag8AHhWQAGg/gNg/IgzjMQg7jmg9jlIgTg0IBLABICUHsIA4C9QAaBYAfAeQAsAoBagWQCKgECKANQAyAGA4AOQAxATAGA+QgEBxicAEQh/AjiBAYQgmAHgiAAQiNAAg+h8gAh1G/QDOAxDAh7QhbgYhKAAQiWAAhTBig");
	this.shape_68.setTransform(438.275,437.1922);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("rgba(255,255,255,0.067)").s().p("AjIAmQCAiOERBPQiGBPiMAAQg+AAhBgQg");
	this.shape_69.setTransform(446.625,478.347);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#000000").s().p("AjtGtQgag8AHhVQAFhAgMg+Ig0jNIh4nLIgTgzIBLAAICTHtIA5C8QAbBYAfAeQAsAoBegQQCKAACJAPQAzAIA4APQAwAVAEA+QgGBwicAAQiAAhiCAUQgjAGgeAAQiSAAg9iBgAh2G/QDPA1DCh1QhhgchPAAQiOAAhTBcg");
	this.shape_70.setTransform(438.425,437.4131);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("rgba(255,255,255,0.067)").s().p("AjJAiQCDiLEPBUQiDBKiFAAQhEAAhGgTg");
	this.shape_71.setTransform(446.85,479.0155);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#000000").s().p("AjtGrQgZg9AGhVQAFg/gNg/Ig0jMIh4nLIgTgzIBKAAICVHsIA5C8QAaBZAfAdQAtAoBigJQCLADCIASQAyAJA4AQQAwAWACA/QgIBwicgEQiCAeiBARQgfAEgaAAQiZAAg8iFgAh2HAQDNA6DFhyQhmgghTAAQiHAAhSBYg");
	this.shape_72.setTransform(438.55,437.6433);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("rgba(255,255,255,0.067)").s().p("AjJAeQCGiIENBbQh/BDiAAAQhKAAhKgWg");
	this.shape_73.setTransform(447.025,479.6646);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#000000").s().p("AjsGoQgYg+AEhUQAFg/gNg/Ig1jMIh5nLIgTgzIBLAAICUHsIA6C8QAbBYAgAdQAsAoBmgDQCKAHCJAWQAxAKA4ARQAvAXACA+QgMBxicgIQiBAbiDAOQgaAEgXAAQifAAg6iLgAh2G/QDMA+DIhsQhsgkhWAAQiBAAhRBSg");
	this.shape_74.setTransform(438.675,437.9069);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38}]}).to({state:[{t:this.shape_41},{t:this.shape_40}]},1).to({state:[{t:this.shape_43},{t:this.shape_42}]},1).to({state:[{t:this.shape_45},{t:this.shape_44}]},1).to({state:[{t:this.shape_47},{t:this.shape_46}]},1).to({state:[{t:this.shape_49},{t:this.shape_48}]},1).to({state:[{t:this.shape_51},{t:this.shape_50}]},1).to({state:[{t:this.shape_53},{t:this.shape_52}]},1).to({state:[{t:this.shape_55},{t:this.shape_54,p:{y:474.4539}}]},1).to({state:[{t:this.shape_57},{t:this.shape_56}]},1).to({state:[{t:this.shape_58},{t:this.shape_54,p:{y:474.4039}}]},1).to({state:[{t:this.shape_60},{t:this.shape_59}]},1).to({state:[{t:this.shape_62},{t:this.shape_61}]},1).to({state:[{t:this.shape_64},{t:this.shape_63}]},1).to({state:[{t:this.shape_66},{t:this.shape_65}]},1).to({state:[{t:this.shape_68},{t:this.shape_67}]},1).to({state:[{t:this.shape_70},{t:this.shape_69}]},1).to({state:[{t:this.shape_72},{t:this.shape_71}]},1).to({state:[{t:this.shape_74},{t:this.shape_73}]},1).to({state:[{t:this.shape_74},{t:this.shape_73}]},1).wait(1));

	// Armature_9
	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("rgba(255,255,255,0.067)").s().p("ABOhFQgeCBh9ALQAuiMBtAAg");
	this.shape_75.setTransform(113.025,492.8);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#000000").s().p("AjiNmQgbgrAQhAIAPglQBIicC5gqQAwgEAgg9QAXgngKg+QgkkRAdkGQAMjLAIjJQACieAGijQApghApA/QAACDgEBrQgJDigNDiQgFBwgBBxQABDUAqDdQAFA2gFA6IgBADQhlDgj1BXQgSAEgRAAQgxAAglgogAhuMbQB9gLAeiCQhtABguCMg");
	this.shape_76.setTransform(116.3432,420.3123);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("rgba(255,255,255,0.067)").s().p("ABShBQglCAh+ADQA2iIBtAFg");
	this.shape_77.setTransform(112.9,490.5912);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#000000").s().p("AjqNTQgagtAUg/IASgkQBQiXC8gfQAwgCAWgvQAWgpgMg9QgtkQATkHQALjJAUjJQAHieAMijQApggAnBAQgECDgHBrQgPDhgVDfQgEBxAHBuQAIDUAyDcQAHA1gDA6IgBADQhdDjkOA/QgOADgOAAQg1AAglgtgAhzMPQB+gEAliAIgHgBQhnAAg1CFg");
	this.shape_78.setTransform(116.2554,418.9425);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("rgba(255,255,255,0.067)").s().p("AhVA9QA/iFBsAMQgsB6h4AAIgHgBg");
	this.shape_79.setTransform(112.825,488.2054);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#000000").s().p("AjyM+QgXguAYg+IATgiQBaiUC8gUQAwABAOghQAUgqgOg9Qg4kOAKkHQALjJAhjJQAMidAQijQAqgeAlBBQgHCDgKBrQgXDhgbDbQgFBwAPBrQAQDUA6DaQAJA1gBA6IgBACQhVDnkmAnIgUACQg8AAgkgzgAh3MBQB+ADAth9IgSgBQhfAAg6B7g");
	this.shape_80.setTransform(116.2261,417.488);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("rgba(255,255,255,0.067)").s().p("AhYA5QBGiCBrASQgwBxhuAAIgTgBg");
	this.shape_81.setTransform(112.825,485.6072);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#000000").s().p("Aj5MoQgUgvAbg9QAKgSALgPQBiiOC9gJQAwAEAFgVQASgrgQg8QhBkLAAkIQAJjIAvjIQAQieAWihQArgdAjBCQgMCCgNBrQgdDggjDXQgEBwAXBpQAXDTBCDXQAKA2ACA5IgBADQhNDqk9AOIgMAAQhBAAgkg4gAh7LyQB9AKA1h7QgOgCgMAAQhaAAg+Bzg");
	this.shape_82.setTransform(116.2867,415.9323);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("rgba(255,255,255,0.067)").s().p("AhbA0QBNh+BqAZQgzBnhlAAQgPAAgQgCg");
	this.shape_83.setTransform(112.925,482.863);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#000000").s().p("Aj/MRQgRgxAeg6IAXghQBpiHC+ABQAwAGgFgHQAQgrgSg8QhLkIgJkIQAJjIA7jGQAVidAbihQArgdAiBFQgQCBgRBqQgkDggpDTQgDBwAdBlQAfDSBKDVQAMA0AEA7IgBACQgiB2h5AzQhuA2iOACIgEAAQhHAAgjg+gAh+LiQB+ASA6h4QgSgEgQAAQhVAAhBBqg");
	this.shape_84.setTransform(116.3926,414.233);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("rgba(255,255,255,0.067)").s().p("AheAuQBUh4BpAeQg3BfhdAAQgTAAgWgFg");
	this.shape_85.setTransform(113.1,479.9084);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#000000").s().p("AibM8QhKgDgghBQgOgyAhg5IAageQBwiCC+AMQAbAGAGAEIgBAFQAHgCgGgDQANgqgTg4QhVkGgSkHQAIjHBIjFQAZicAgigQAtgbAeBFQgTCBgUBqQgrDegwDPQgDBvAlBiQAnDQBRDTQAOA0AGA6IgBACQgeB4iBAqQhlAph6AAIgmgBgAiBLQQB8AZBBh0QgVgGgUAAQhRAAhDBhg");
	this.shape_86.setTransform(116.5665,412.5161);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("rgba(255,255,255,0.067)").s().p("AhgApQBbhzBmAjQg4BXhWAAQgYAAgbgHg");
	this.shape_87.setTransform(113.325,476.8134);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#000000").s().p("AijMnQhKgIgchDQgMgyAlg3IAbgdQB4h7C9AXQAXAGgEAYQAAAQACgQQANgsgWg7QhekCgbkHQAIjGBTjDQAfibAkifQAugaAcBGQgYCBgWBpQgyDcg3DLQgDBuAtBfQAuDPBZDPQAQA0AIA6IgBACQgaB4iIAiQhYAchjAAQgoAAgqgEgAiDK9QB6AgBIhxQgZgIgXAAQhMAAhGBZg");
	this.shape_88.setTransform(116.7781,410.83);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("rgba(255,255,255,0.067)").s().p("AhjAjQBihuBkAqQg5BOhQAAQgdAAgggKg");
	this.shape_89.setTransform(113.65,473.5311);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#000000").s().p("AiqMPQhKgMgYhEQgIgzAng1IAdgbQB/h0C6AiQAKADAEAHQABASgFAQQABAPADgVQAGgSgGgKQgBgfgPglQhnj/glkFQAHjFBgjCQAkiZAoifQAvgYAaBHQgbCAgaBoQg4Dbg/DGQgDBtA1BcQA2DNBgDMQASAzAKA6IgBACQgWB5iQAZQhHAShMAAQg+AAhBgLgAiEKnQB4AmBOhrQgcgMgcAAQhIAAhGBRg");
	this.shape_90.setTransform(117.0326,409.1733);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("rgba(255,255,255,0.067)").s().p("AhkAdQBmhoBjAwQg6BGhKAAQghAAgkgOg");
	this.shape_91.setTransform(114.025,470.1071);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#000000").s().p("AixL1QhJgQgUhGQgFgzAqgyIAegaQCEhsC3AsQAIAggHAbQACANAIgaQAOglgWgJIgDAAQgEgUgLgXQhvj7gvkEQAHjCBrjBQApiZAticQAvgXAZBIQggB/gdBnQgfBsgyBhQgpBkgKBqQgCBsA8BYQA9DLBnDIQAUAzAMA5QAAABgBAAQAAAAAAAAQAAAAAAAAQAAABAAAAQgRB6iWAPQg3AKg6AAQhRAAhXgTgAiFKPQB2AuBThnQgfgPgfAAQhEAAhHBIg");
	this.shape_92.setTransform(117.3237,407.5066);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("rgba(255,255,255,0.067)").s().p("AhmAXQBthiBgA1Qg7A/hFAAQglAAgogSg");
	this.shape_93.setTransform(114.475,466.5273);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#000000").s().p("Ai3LZQhHgUgRhHQgCgzAtgwIAfgYQCDhfCnArQARArgHAkQAEAMAMgfQATgsgWgKIgXgGIgKgWIh5j7Qg0h/gEh+QADhhAwhfQAnhfAlhhQAtiXAyicQAvgVAXBIQgjB+ghBnQgjBrg2BeQguBigJBpQgCBqBCBVQBEDJBvDEQAWAxANA6QAAAAAAAAQAAAAAAAAQAAABAAAAQAAAAAAAAQgNB7idAGQglAFgmAAQhlAAhtgfgAiGJ2QB0A0BZhiQgigTgjAAQhAAAhIBBg");
	this.shape_94.setTransform(117.6707,405.8817);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#000000").s().p("AixL1QhJgQgUhGQgGgzArgyIAegaQCEhsC3AsQAIAfgHAcQACANAIgaQAOglgWgJIgDAAQgEgVgLgWQhvj7gvkEQAHjCBrjBQApiZAticQAvgXAZBIQggB/gdBnQgfBsgyBhQgpBkgKBqQgCBsA8BYQA9DLBnDIQAUAzAMA5QAAAAgBABQAAAAAAAAQAAAAAAAAQAAAAAAABQgRB6iXAQQg2AKg5AAQhRAAhYgUgAiFKPQB2AuBThnQgfgPgfAAQhEAAhHBIg");
	this.shape_95.setTransform(117.3133,407.5273);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("rgba(255,255,255,0.067)").s().p("AhiAjQBhhuBlAqQg6BOhQAAQgdAAgfgKg");
	this.shape_96.setTransform(113.6,473.5409);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#000000").s().p("AiqMPQhKgLgYhFQgJgzAog0IAdgcQB/h0C7AiQAKADAEAIQAAARgFAQQABAPADgVQAFgRgEgKQgBgggQglQhnj/gkkFQAHjFBfjCQAkiZApifQAugYAbBHQgcCAgaBoQg4Dbg+DGQgDBtA0BcQA2DNBgDMQASA0AKA5QgBAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQgVB5iQAZQhHAShNAAQg9AAhCgLgAiEKnQB4AnBOhsQgcgMgcAAQhIAAhGBRg");
	this.shape_97.setTransform(116.9923,409.1733);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("rgba(255,255,255,0.067)").s().p("AhgApQBbhzBmAkQg4BWhVAAQgZAAgbgHg");
	this.shape_98.setTransform(113.275,476.8292);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#000000").s().p("AijMnQhKgHgchDQgMgyAlg3IAbgeQB4h7C9AYQAXAGgEAXQAAAQACgQQAOgtgXg6QhdkCgbkHQAHjGBTjDQAgibAjifQAugaAdBGQgYCBgXBpQgyDcg3DLQgDBuAtBfQAuDPBZDPQAQA0AIA6IgBACQgaB5iJAiQhWAbhiAAQgpAAgrgEgAiDK9QB6AgBIhwQgZgJgYAAQhLAAhGBZg");
	this.shape_99.setTransform(116.7281,410.8423);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("rgba(255,255,255,0.067)").s().p("AheAvQBVh5BoAeQg2BfhdAAQgUAAgWgEg");
	this.shape_100.setTransform(113,479.9488);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#000000").s().p("AiaM8QhKgDgghBQgPgxAig5IAZgfQBxiBC9AMQAbAFAHAEIgCAFQAHgDgFgCQAMgqgTg4QhUkGgSkHQAIjHBIjFQAZicAgigQAtgbAeBFQgTCBgUBqQgrDegwDPQgDBvAlBiQAmDRBRDSQAPA0AFA6IgBACQgeB4iAArQhmAoh6AAIglgBgAiBLRQB8AZBBh1QgVgGgUAAQhQAAhEBig");
	this.shape_101.setTransform(116.4954,412.5411);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("rgba(255,255,255,0.067)").s().p("AhbA0QBNh9BqAYQgzBnhlAAQgPAAgQgCg");
	this.shape_102.setTransform(112.825,482.8754);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#000000").s().p("Aj/MRQgRgxAeg6IAXghQBqiHC9ABQAwAHgGgIQASgsgTg7QhLkJgIkIQAJjIA7jGQAVicAaiiQAsgcAhBEQgPCBgRBrQgkDfgqDTQgDBwAeBlQAfDSBJDVQANA2ADA5IgBACQgjB3h4AzQhuA2iPACIgDAAQhIAAgig+gAh+LiQB9ASA6h4QgRgEgQAAQhVAAhBBqg");
	this.shape_103.setTransform(116.3426,414.258);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("rgba(255,255,255,0.067)").s().p("AhYA5QBFiCBsASQgwBxhuAAIgTgBg");
	this.shape_104.setTransform(112.75,485.6572);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#000000").s().p("Aj5MoQgUgvAbg9IAVghQBiiOC9gJQAwADAFgVQATgqgRg8QhBkMABkIQAKjIAujIQAQidAWiiQArgdAjBDQgMCCgNBqQgdDhgjDXQgEBwAWBoQAYDUBBDXQALA1ABA6IgBACQhNDqk9APIgMAAQhBAAgkg4gAh6LyQB8AKA1h7QgOgCgMAAQhbAAg8Bzg");
	this.shape_105.setTransform(116.1867,415.9573);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("rgba(255,255,255,0.067)").s().p("AhVA9QA/iFBsAMQgtB6h3AAIgHgBg");
	this.shape_106.setTransform(112.725,488.2554);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#000000").s().p("AjyM+QgXguAYg+IATgiQBaiTC8gVQAxABANgiQAUgqgOg8Qg3kOAKkIQALjJAhjIQAMieAQiiQAqgfAlBCQgHCCgKBrQgXDhgbDbQgFBwAPBsQAQDUA5DZQAJA2gBA6IgBACQhWDnklAnIgUACQg7AAglgzgAh3MBQB9ADAuh9IgSgBQhfAAg6B7g");
	this.shape_107.setTransform(116.105,417.513);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("rgba(255,255,255,0.067)").s().p("ABShBQgmCAh9ADQA3iIBsAFg");
	this.shape_108.setTransform(112.775,490.6412);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#000000").s().p("AjqNTQgZgtATg/IASgjQBRiYC7gfQAwgCAXgvQAWgpgMg+QgukPAUkHQAMjKAUjJQAHieALijQAqggAnBBQgFCCgGBsQgQDhgUDfQgFBwAHBvQAIDTAyDcQAGA2gDA6IgBACQheDkkMA/QgPADgOAAQg1AAglgtgAhzMPQB+gEAmiAIgIAAQhnAAg1CEg");
	this.shape_109.setTransform(116.1218,418.9675);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("rgba(255,255,255,0.067)").s().p("ABOhGQgeCBh9AMQAuiLBtgCg");
	this.shape_110.setTransform(112.9,492.825);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#000000").s().p("AjiNnQgbgsAQhAIAPgkQBIidC5gqQAwgEAgg9QAXgngKg+QgkkRAekGQAMjKAIjKQACieAGijQApghApA/QAACDgEBrQgJDigNDiQgFBwgBBxQAADVAqDdQAFA2gGA5IgBADQhmDgjzBXQgSAFgRAAQgxAAglgogAhuMcQB9gMAeiCQhtACguCMg");
	this.shape_111.setTransform(116.232,420.3373);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_76},{t:this.shape_75}]}).to({state:[{t:this.shape_78},{t:this.shape_77}]},1).to({state:[{t:this.shape_80},{t:this.shape_79}]},1).to({state:[{t:this.shape_82},{t:this.shape_81}]},1).to({state:[{t:this.shape_84},{t:this.shape_83}]},1).to({state:[{t:this.shape_86},{t:this.shape_85}]},1).to({state:[{t:this.shape_88},{t:this.shape_87}]},1).to({state:[{t:this.shape_90},{t:this.shape_89}]},1).to({state:[{t:this.shape_92},{t:this.shape_91,p:{y:470.1071}}]},1).to({state:[{t:this.shape_94},{t:this.shape_93}]},1).to({state:[{t:this.shape_95},{t:this.shape_91,p:{y:470.1216}}]},1).to({state:[{t:this.shape_97},{t:this.shape_96}]},1).to({state:[{t:this.shape_99},{t:this.shape_98}]},1).to({state:[{t:this.shape_101},{t:this.shape_100}]},1).to({state:[{t:this.shape_103},{t:this.shape_102}]},1).to({state:[{t:this.shape_105},{t:this.shape_104}]},1).to({state:[{t:this.shape_107},{t:this.shape_106}]},1).to({state:[{t:this.shape_109},{t:this.shape_108}]},1).to({state:[{t:this.shape_111},{t:this.shape_110}]},1).to({state:[{t:this.shape_111},{t:this.shape_110}]},1).wait(1));

	// Armature_7
	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#000000").s().p("AjLJ+QgcgDgZg1QgMh3AyhvQAcgWALgoIBglFICnpCQAGgPgLgiQBRgNBjA0QhZA1gdCNQgxDFg+DCQggBsgfBuQgZBZgKBlQBTApA3AZQA9AkA2AsQBaBhh/AzIhAACQiNAAiSgcg");
	this.shape_112.setTransform(180.45,413.2715);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#000000").s().p("AjGKCQgcgCgag1QgRh3AvhwQAbgXAKgoQAqilAsijQBUkgBhkeQAGgOgKgjQBRgLBiA2QhaAzggCMQg2DEhDC+QgfBsgZBsQgXBagHBlQBUAnA4AXQA/AiA3ArQBdBeh+A3Qg2AEg4AAQh2AAh7gTg");
	this.shape_113.setTransform(180.6106,412.8206);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#000000").s().p("AjBKEQgbgCgdgzQgUh3AshxQAagYAJgpIBMlKQBSkgBxkZQAGgOgJgjQBSgIBgA4QhbAxgkCLQg7DChHC5QggBsgSBqQgUBbgEBlQBWAkA4AWQA/AhA5AoQBgBbh8A7QhOAIhPAAQhfAAhkgMg");
	this.shape_114.setTransform(180.735,412.405);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#000000").s().p("Ai8KFQgbgBgegzQgXh1AnhzQAagYAHgpIBClNQBSkgB/kUQAHgOgIgjQBSgHBfA7QhdAvgnCKQhADBhLCzQggBsgMBpQgRBbgBBmQBWAhA6AUQBAAeA6AnQBiBZh6A+QhkAOhmAAQhKAAhMgHg");
	this.shape_115.setTransform(180.8768,412.0245);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#000000").s().p("Ai2KFQgcAAgfgyQgbh0Akh0QAagaAGgpQAbioAcimQBRkgCOkPQAHgOgHgjQBSgFBeA9QhfAtgqCJQhEC/hRCvQgfBrgGBoQgPBbADBlQBXAfA6ASQBCAdA7AlQBkBVh4BCQh7AViAAAQgyAAgygDg");
	this.shape_116.setTransform(180.9842,411.6377);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#000000").s().p("AiwKDQgcABghgxQgeh0Ahh0QAYgaAFgqIAtlPQBRkfCckLQAIgNgGgkQBSgCBbA/QhfAqgtCIQhKC+hVCpQgfBsABBlQgMBcAFBlQBZAcA6ARQBCAaA8AjQBnBSh1BGQiUAdicAAIgvgBg");
	this.shape_117.setTransform(181.08,411.2798);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#000000").s().p("AjoJSQgihzAeh1QAXgbAEgqQAQipATioQBPkdCrkGQAIgNgFgkQBSAABaBCQhgAngxCHQhOC8haCkQgfBrAHBjQgJBdAJBkQBZAaA7AOQBDAZA9AhQBqBPh0BJQipAni1AAIgCABQgcAAgggvg");
	this.shape_118.setTransform(181.1523,410.9559);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#000000").s().p("AjiJRQgmhyAah3QAWgbADgqIAZlSQBNkcC7kAQAIgOgFgjQBTABBYBEQhhAmg0CFQhTC6hfCeQgeBrANBhQgGBdAMBkQBaAXA7ANQBDAWA/AgQBrBMhxBMQioAti0AFIgEAAQgaAAghgsg");
	this.shape_119.setTransform(181.2339,410.609);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#000000").s().p("AjdJPQgphxAWh3QAWgdABgpIAPlSQBMkcDJj6QAJgOgEgjQBSAEBXBGQhiAig4CFQhXC4hjCYQgdBrASBeQgDBdAPBkQBaAVA8AKQBEAVA/AdQBvBIhwBRQilAxi1ALIgFAAQgaAAgigqg");
	this.shape_120.setTransform(181.2909,410.216);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#000000").s().p("AjYJMQgshvASh4QAVgdAAgqIAElSQAmiNBah/ICjkDQAJgMgDgkQBSAFBVBJQhjAfg7CEQhbC1hpCTQgdBqAaBdQgBBdASBjICXAaQBEATBBAbQBwBFhsBUQilA2i0AQIgFAAQgaAAgjgog");
	this.shape_121.setTransform(181.4021,409.7957);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#000000").s().p("AjdJPQgphxAWh3QAVgdACgpIAOlSQBNkcDJj6QAIgOgDgjQBSAEBWBGQhhAig4CFQhXC4hkCYQgdBrATBeQgDBdAOBkQBbAVA8AKQBDAVBAAdQBuBIhvBRQimAxi1ALIgEAAQgaAAgigqg");
	this.shape_122.setTransform(181.3128,410.216);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#000000").s().p("AjiJRQgmhyAah3QAXgbACgqQAMiqANioQBNkcC7kAQAIgOgFgjQBTABBYBEQhhAmg0CFQhTC6hfCeQgeBrANBhQgGBdAMBkQBaAXA7ANQBDAWA/AgQBsBLhxBNQioAsi1AGIgDAAQgbAAghgsg");
	this.shape_123.setTransform(181.2469,410.609);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#000000").s().p("AjnJSQgihyAdh2QAXgbAEgqIAjlRQBPkdCrkGQAIgNgFgkQBSAABaBCQhgAngxCHQhOC8haCkQgeBrAGBjQgJBdAJBkQBZAaA7AOQBCAYA+AiQBqBPh0BJQioAni1AAIgDABQgbAAgggvg");
	this.shape_124.setTransform(181.195,410.9559);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#000000").s().p("AiwKDQgbABghgxQgfhzAhh1QAYgaAFgqIAtlPQBPkfCekLQAHgNgGgkQBSgCBcA/QhfAqguCIQhJC+hVCpQggBsABBlQgLBcAFBlQBZAcA6AQQBCAbA9AjQBnBSh2BGQiSAdiaAAIgzgBg");
	this.shape_125.setTransform(181.1117,411.2852);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#000000").s().p("Ai1KFQgcAAgfgyQgch1AlhzQAZgaAGgpIA3lOQBQkgCPkPQAHgOgHgjQBSgFBdA9QheAtgqCJQhEC/hRCuQggBsgFBnQgPBcADBlQBYAfA6ASQBAAcA8AlQBlBVh4BDQh9AViBAAQgwAAgxgDg");
	this.shape_126.setTransform(181.0342,411.6259);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#000000").s().p("Ai7KFQgbAAgeg0QgYh1AohzQAZgYAIgpQAfinAiimQBRkgCAkUQAHgOgIgjQBSgHBeA7QhdAvgmCKQhBDBhLCzQggBsgLBpQgRBbgBBlQBWAiA6AUQBAAeA7AnQBhBYh5A+QhmAPhoAAQhIAAhKgHg");
	this.shape_127.setTransform(180.925,412.0039);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#000000").s().p("AjAKEQgcAAgcg1QgUh1ArhyQAagYAJgpIBLlKQBSkhBxkZQAGgOgJgjQBSgIBgA4QhbAxgkCLQg7DDhHC4QgfBsgTBrQgTBbgEBlICOA5QA/AgA6ApQBfBbh7A7QhPAIhQAAQhfAAhigMg");
	this.shape_128.setTransform(180.8075,412.3851);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#000000").s().p("AjFKDQgbgCgbg1QgRh3AvhwQAbgXAKgoQApilArijQBUkiBhkdQAGgOgJgjQBRgKBiA2QhbAzggCMQg2DEhDC9QgfBsgYBsQgXBagGBlQBUAnA4AXQA/AiA3AqQBdBeh9A3Qg6AFg6AAQh0AAh4gSg");
	this.shape_129.setTransform(180.6839,412.7895);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#000000").s().p("AjJJ/QgcgDgZg1QgNh3AyhvQAcgWALgoIBelGICnpCQAGgPgLgiQBRgMBjAzQhZA2gcCMQgyDGg+DBQggBsgeBuQgZBZgKBlQBTApA4AZQA9AkA3AsQBZBgh+A0QgkABgkAAQiJAAiOgag");
	this.shape_130.setTransform(180.549,413.24);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_112}]}).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128}]},1).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_130}]},1).to({state:[{t:this.shape_130}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(342.6,445.2,221.39999999999998,66.19999999999999);
// library properties:
lib.properties = {
	id: 'EF9EDB0F610A4DC6AEDA0A22625FE4FB',
	width: 600,
	height: 600,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['EF9EDB0F610A4DC6AEDA0A22625FE4FB'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;