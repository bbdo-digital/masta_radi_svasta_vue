(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol_13_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgnBDQAahaArg4QALAKgCANQAAAFgHAQIgTAtQgKAbAAAXQAAASgIACIgCAAQgKAAgWgNg");
	this.shape.setTransform(11.5013,23.1747,2.9053,2.9053);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_13_Layer_1, null, null);


(lib.Symbol_12_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZBOQgIgGACgPQAKg1AVhHQAAgEAHgDIANgHIAIASQAFAKgBAGQgHAagLAlIgTBAIgIACQgHAAgFgEg");
	this.shape.setTransform(9.3566,23.6644,2.9053,2.9053);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_12_Layer_1, null, null);


(lib.Symbol_11_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgIBYQgEAAgFgGQgGgFAAgEQAHg0AQhnQABgCAMgDIAHAPQAEAJAAAGQgFA/gLBFQgBAEgGAFQgEAEgEAAIgBAAg");
	this.shape.setTransform(6.8999,25.6347,2.9053,2.9053);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_11_Layer_1, null, null);


(lib.Symbol_10_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgLhXIAPATQAGAIAAAEQADBJgBAxQAAAEgHAEIgQAOg");
	this.shape.setTransform(3.4695,25.587,2.9053,2.9053);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_10_Layer_1, null, null);


(lib.Symbol_9_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgVhHQAVANAJAOQAJAOgBAVQAAAQAEAjQACAPgFAHQgHAIgQAAg");
	this.shape.setTransform(6.3015,20.8359,2.9053,2.9053);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_9_Layer_1, null, null);


(lib.Symbol_8_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AADBWIghitQAgAZASAwQASAvgLAhQgGAWgNAAIgFgCg");
	this.shape.setTransform(9.0101,25.4205,2.9053,2.9053);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_8_Layer_1, null, null);


(lib.Symbol_5_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("ADUNyQg2gJgbgxQgrhQgChbQgBgpgIhHQgJhQgCggQgLjAgChIQgEiVAGhzIgDgZQhsAJhdg3QgHBLAHBZQADAzAPBrQAMBRAQCoQABAJgCAKQgCAMgEACQgQAIgIAUIgOAkQgaAxgVAdQgLAPgYAKQgSAHgLgKQgKgLAFgVQAZhtBAguQAUgPAHgRQAIgSgCgYIgIhhQgEg8gEgkQgOhqgEgrQgHhVAEhDQACgTgGgLQgGgMgPgIQhWgtgphqQgnhnAaheQALgnAeg1QAjg7AQgeIAIgKIAHgKQA5hnCBgMQBHgHAUgDQAlgFAlAPQAbAKAoAbQBtBLApB7QAjBqgPCWQgFA1gZAwQgXAvgnAkQgQAQgdAOIgzAVQgMAFgUAEIgmAIIgIBLQgFAsABAeQADD0AJDEQABAhAGAwIAMBQQADAVAMATQAJAOASASQAfAfA3BBQAGAIAJAYQAKAggMAQQgJANgUAAIgMgBgACeMdQARAeAiAKQgBgjgWgbIgugsQAHAsALAWgAjbIUIAJAFIAXgeQAKgSgFgXgAANtGQgoAEgmAIQgwALgZANQgmATgUAlIgvBVIgtBUQgsBXAnBuQAqB2BgAkQAVAIARAMQA2AnBHgBQBMgCApgJQBfgUA0hSQAYglAKgzQAFgbAFg/QgFiDgrhWQgwhghlgwQgngTgtAAIgWABg");
	this.shape.setTransform(104.4435,259.3259,2.9365,2.9365);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_5_Layer_1, null, null);


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AANAsQgYgFgigKIg6gTQgTgGgIgKQgLgOADgZIAvARQAbAKATAFIAtANQAaAFATgCQATgBAbgKIAtgTIADAIIABAFQgwA8g6AAQgLAAgKgCg");
	this.shape.setTransform(41.0026,13.556,2.9365,2.9365);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_4_Layer_1, null, null);


(lib.Symbol_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgaAVQgGgIADgKQACgJAIgIQAIgJAJgDQACgCAKAJIAVATIgWAQQgKAIgHACIgGABQgHAAgFgGg");
	this.shape.setTransform(9.0632,7.9595,2.9362,2.9362);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AghAQQAJgNALgUQALgOAOAGQAjAOgUAng");
	this.shape_1.setTransform(38.5314,15.7493,2.9362,2.9362);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_2_Layer_1, null, null);


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AFvdYQiXgygOiAQgLh1BkiZQAagoAKgWQAOggABgjQAEh8gkiHQgbhhg8iRQgJgXgggTQgdgSgcgDQhTgMhRAYQhoAjhrgaQhYgVhqhFQgcgTgvgWQg4gZgfgQQhYEMgsFIQgDAUAMAiIAVA+IDMhlQCEhDB9BtQBJBBACBZQABBbhMAxQivB1idgBQi8gDiXiyQA5g2AWhUQANgvAKhvQAbjoA/l3QANhYgfhDQkCoUDencIAzh0QiLgyg9goQhohDAFiaIA3gIQAhgGAXgKQB8g1AxgmQBIg2BCgLQBIgNBLAoQAbANBDAHQA0jMAchhQBQjvAlh4QASg0AggUQAdgVAyADQBZAGCzAGIELAMQBFAEBPAdQBNAhATAtQATAsgWBbQiBIRghCKQgNA/gGCNQCCAXBxBnQAyAsCHCiQAUAXAPAlQAQAmgHAQQgPAcggAVQghAWggAEQgsAJhCgDQhMgDgoAAQApEVh3DvQhZC5jaDTQAfB8BCD2QAvDagTCzICyASQBkAKBGADQBLADA+AsQBAAtAYBIQAWBBgcBAQgcBCg7ARQiwAvheAKQguAFgsAAQhjAAhUgbgAFwZSQgkAMgFA3QA0BoBugDQBTgCEDgeQAfgFAZgiQAWgcgSgdQgQgcgpgHIg5BnIhBglQgcgWgfgfQhZBOgcADQgvAEgyhpQg+AAgJACgAq3XDIAJAnICeAPIhLiJgAn6VMIBrCIIBnhTQgzhkgigJIgJgBQgkAAhQA5gAp4pZQgZAegNAWQh2DAAcE2QAYETBlCOQBaB9BxBIQB9BOCEgDQDAgEBsglQChg2BbiTQBnioAvhmQBbjUhficQiNgwgMgDQgjgLgOgBQgbgEgRAHQgqAWg8AoQhIAwgrAaICRBEQALAGAqAPQAhALARAMQAoAdAEAuQADAvgjAeQiGByg2AmQhYBAhHANQhaARhVgxQh6hIg9jOQg+jNA7iKIBIixIkRiWQgOASgdAegApgw8QhNAJhZBxQAIAbACABQAHAHAxAlQEiDfHOCVQCNAtDAAyQBwAdDgA3QA9AOAggCQA1gFAngwQhchzhRg2QhkhEh0ACQhABfgpAKQgpAJhag6QgGgFhPgtQjKhpgigQQihhIgCgBQhggghRAUQgMADgTgKIgjgTIAkilQhnhPhIAAIgPABgAjkvIQAQAZAzAVQAGADAvAMQAhAIAQAOQCUB/D2AxIBLhuIixguQhlgag+gYQhAgXhdguIilhOQAGBEASAagAEF7oQiBAGg/AAQhNAAglAYQgqAagZBJQgrB/hFEGQB6BzCOBLQCOBLCKAWQBYAOAggbQAhgbAOheQAJgyAyjZIBbl2QhDgSgfgHQg0gMghAAQhBAAiAAHg");
	this.shape.setTransform(102.9472,190.7245);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_1_Layer_1, null, null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_13_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(11.5,23.2,1,1,0,0,0,11.5,23.2);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(0,0,23,46.4), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_12_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(9.3,23.7,1,1,0,0,0,9.3,23.7);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(0,0,18.7,47.4), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_11_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(6.9,25.6,1,1,0,0,0,6.9,25.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,13.8,51.3), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_10_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(3.5,25.6,1,1,0,0,0,3.5,25.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,7,51.2), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_9_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(6.3,20.9,1,1,0,0,0,6.3,20.9);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,12.6,41.7), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_8_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(9,25.4,1,1,0,0,0,9,25.4);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,18,50.9), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_5_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(104.4,259.3,1,1,0,0,0,104.4,259.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,208.9,518.6), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(41,13.6,1,1,0,0,0,41,13.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,82,27.1), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(24.3,12,1,1,0,0,0,24.3,12);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,48.7,24), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(103,190.7,1,1,0,0,0,103,190.7);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,205.9,381.5), null);


(lib.Scene_1_right = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// right
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(738.7,340.1,1,1,0,0,0,103,190.7);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(33).to({_off:false},0).to({x:384.75,alpha:1},12,cjs.Ease.backOut).wait(57).to({y:566.1,alpha:0},11,cjs.Ease.backIn).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_mouth = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mouth
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(185.7,197.4,1,1,0,0,0,41,13.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(23).to({_off:false},0).wait(97));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_left = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// left
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(211.6,-270.5,1,1,0,0,0,104.4,259.3);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(31).to({_off:false},0).to({y:300.5,alpha:1},10,cjs.Ease.backOut).wait(48).to({y:645.5,alpha:0},13,cjs.Ease.backIn).wait(18));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_16
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(165.4,39.75,1,1,0,0,0,9.3,23.7);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off:false},0).to({y:143.75,alpha:1},7,cjs.Ease.quadOut).wait(94).to({y:262.75,alpha:0},10,cjs.Ease.backIn).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_15
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(249.65,61.2,1,1,0,0,0,9,25.4);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({_off:false},0).to({y:165.2,alpha:1},7,cjs.Ease.quadOut).wait(86).to({y:284.2,alpha:0},10,cjs.Ease.backIn).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(186.7,47.45,1,1,0,0,0,6.9,25.6);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off:false},0).to({y:151.45,alpha:1},7,cjs.Ease.quadOut).wait(91).to({y:270.45,alpha:0},10,cjs.Ease.backIn).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(142.2,37.5,1,1,0,0,0,11.5,23.2);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({_off:false},0).to({y:141.5,alpha:1},7,cjs.Ease.quadOut).wait(97).to({y:260.5,alpha:0},10,cjs.Ease.backIn).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(211.9,54.1,1,1,0,0,0,3.5,25.6);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13).to({_off:false},0).to({y:158.1,alpha:1},7,cjs.Ease.quadOut).wait(89).to({y:277.1,alpha:0},10,cjs.Ease.backIn).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(228.5,54.95,1,1,0,0,0,6.3,20.9);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(15).to({_off:false},0).to({y:158.95,alpha:1},7,cjs.Ease.quadOut).wait(87).to({y:277.95,alpha:0},10,cjs.Ease.backIn).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_eyes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// eyes
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(209.6,-39.85,1,1,0,0,0,24.3,12);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:80.15,alpha:1},10,cjs.Ease.backOut).wait(103).to({y:546.1,alpha:0},6,cjs.Ease.backIn).wait(1));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.Mustache_animation_600x600 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_119 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(119).call(this.frame_119).wait(1));

	// Layer_15_obj_
	this.Layer_15 = new lib.Scene_1_Layer_15();
	this.Layer_15.name = "Layer_15";
	this.Layer_15.parent = this;
	this.Layer_15.depth = 0;
	this.Layer_15.isAttachedToCamera = 0
	this.Layer_15.isAttachedToMask = 0
	this.Layer_15.layerDepth = 0
	this.Layer_15.layerIndex = 0
	this.Layer_15.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_15).wait(120));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 1
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(120));

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 2
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(120));

	// Layer_14_obj_
	this.Layer_14 = new lib.Scene_1_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 3
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(120));

	// Layer_16_obj_
	this.Layer_16 = new lib.Scene_1_Layer_16();
	this.Layer_16.name = "Layer_16";
	this.Layer_16.parent = this;
	this.Layer_16.depth = 0;
	this.Layer_16.isAttachedToCamera = 0
	this.Layer_16.isAttachedToMask = 0
	this.Layer_16.layerDepth = 0
	this.Layer_16.layerIndex = 4
	this.Layer_16.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_16).wait(120));

	// Layer_13_obj_
	this.Layer_13 = new lib.Scene_1_Layer_13();
	this.Layer_13.name = "Layer_13";
	this.Layer_13.parent = this;
	this.Layer_13.depth = 0;
	this.Layer_13.isAttachedToCamera = 0
	this.Layer_13.isAttachedToMask = 0
	this.Layer_13.layerDepth = 0
	this.Layer_13.layerIndex = 5
	this.Layer_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_13).wait(120));

	// eyes_obj_
	this.eyes = new lib.Scene_1_eyes();
	this.eyes.name = "eyes";
	this.eyes.parent = this;
	this.eyes.setTransform(209.6,-39.9,1,1,0,0,0,209.6,-39.9);
	this.eyes.depth = 0;
	this.eyes.isAttachedToCamera = 0
	this.eyes.isAttachedToMask = 0
	this.eyes.layerDepth = 0
	this.eyes.layerIndex = 6
	this.eyes.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.eyes).wait(120));

	// mouth_mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_23 = new cjs.Graphics().p("ApBQeIAAnMITNAAIAAHMg");
	var mask_graphics_24 = new cjs.Graphics().p("AmzQtIAAnMITNAAIAAHMg");
	var mask_graphics_25 = new cjs.Graphics().p("Ak3Q5IAAnMITNAAIAAHMg");
	var mask_graphics_26 = new cjs.Graphics().p("AjOREIAAnNITMAAIAAHNg");
	var mask_graphics_27 = new cjs.Graphics().p("Ah5RMIAAnMITNAAIAAHMg");
	var mask_graphics_28 = new cjs.Graphics().p("Ag3RTIAAnMITNAAIAAHMg");
	var mask_graphics_29 = new cjs.Graphics().p("AgHRYIAAnMITNAAIAAHMg");
	var mask_graphics_30 = new cjs.Graphics().p("AAURaIAAnMITPAAIAAHMg");
	var mask_graphics_31 = new cjs.Graphics().p("AAeRbIAAnMITOAAIAAHMg");
	var mask_graphics_102 = new cjs.Graphics().p("AAeRbIAAnMITOAAIAAHMg");
	var mask_graphics_103 = new cjs.Graphics().p("ABORbIAAnMITOAAIAAHMg");
	var mask_graphics_104 = new cjs.Graphics().p("AB+RbIAAnMITNAAIAAHMg");
	var mask_graphics_105 = new cjs.Graphics().p("ACtRbIAAnMITOAAIAAHMg");
	var mask_graphics_106 = new cjs.Graphics().p("ADdRbIAAnMITOAAIAAHMg");
	var mask_graphics_107 = new cjs.Graphics().p("AEMRbIAAnMITOAAIAAHMg");
	var mask_graphics_108 = new cjs.Graphics().p("AE8RbIAAnMITOAAIAAHMg");
	var mask_graphics_109 = new cjs.Graphics().p("AFsRbIAAnMITOAAIAAHMg");
	var mask_graphics_110 = new cjs.Graphics().p("AGcRbIAAnMITOAAIAAHMg");
	var mask_graphics_111 = new cjs.Graphics().p("AHLRbIAAnMITOAAIAAHMg");
	var mask_graphics_112 = new cjs.Graphics().p("AH7RbIAAnMITOAAIAAHMg");
	var mask_graphics_113 = new cjs.Graphics().p("AIrRbIAAnMITOAAIAAHMg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(23).to({graphics:mask_graphics_23,x:65.175,y:105.45}).wait(1).to({graphics:mask_graphics_24,x:79.425,y:106.875}).wait(1).to({graphics:mask_graphics_25,x:91.775,y:108.1}).wait(1).to({graphics:mask_graphics_26,x:102.25,y:109.15}).wait(1).to({graphics:mask_graphics_27,x:110.8,y:110}).wait(1).to({graphics:mask_graphics_28,x:117.45,y:110.675}).wait(1).to({graphics:mask_graphics_29,x:122.2,y:111.15}).wait(1).to({graphics:mask_graphics_30,x:125.05,y:111.425}).wait(1).to({graphics:mask_graphics_31,x:126,y:111.525}).wait(71).to({graphics:mask_graphics_102,x:126,y:111.525}).wait(1).to({graphics:mask_graphics_103,x:130.775,y:111.525}).wait(1).to({graphics:mask_graphics_104,x:135.55,y:111.525}).wait(1).to({graphics:mask_graphics_105,x:140.3,y:111.525}).wait(1).to({graphics:mask_graphics_106,x:145.075,y:111.525}).wait(1).to({graphics:mask_graphics_107,x:149.85,y:111.525}).wait(1).to({graphics:mask_graphics_108,x:154.625,y:111.525}).wait(1).to({graphics:mask_graphics_109,x:159.4,y:111.525}).wait(1).to({graphics:mask_graphics_110,x:164.175,y:111.525}).wait(1).to({graphics:mask_graphics_111,x:168.925,y:111.525}).wait(1).to({graphics:mask_graphics_112,x:173.7,y:111.525}).wait(1).to({graphics:mask_graphics_113,x:178.475,y:111.525}).wait(7));

	// mouth_obj_
	this.mouth = new lib.Scene_1_mouth();
	this.mouth.name = "mouth";
	this.mouth.parent = this;
	this.mouth.depth = 0;
	this.mouth.isAttachedToCamera = 0
	this.mouth.isAttachedToMask = 0
	this.mouth.layerDepth = 0
	this.mouth.layerIndex = 7
	this.mouth.maskLayerName = 0

	var maskedShapeInstanceList = [this.mouth];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.mouth).wait(120));

	// left_obj_
	this.left = new lib.Scene_1_left();
	this.left.name = "left";
	this.left.parent = this;
	this.left.depth = 0;
	this.left.isAttachedToCamera = 0
	this.left.isAttachedToMask = 0
	this.left.layerDepth = 0
	this.left.layerIndex = 8
	this.left.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.left).wait(120));

	// right_obj_
	this.right = new lib.Scene_1_right();
	this.right.name = "right";
	this.right.parent = this;
	this.right.depth = 0;
	this.right.isAttachedToCamera = 0
	this.right.isAttachedToMask = 0
	this.right.layerDepth = 0
	this.right.layerIndex = 9
	this.right.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.right).wait(120));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(407.2,-229.8,434.40000000000003,1134.6);
// library properties:
lib.properties = {
	id: 'BAD478DF423147B1BDA444FFD14208DC',
	width: 600,
	height: 600,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['BAD478DF423147B1BDA444FFD14208DC'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;