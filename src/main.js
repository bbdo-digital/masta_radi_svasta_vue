import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/css/tailwind.css'
import * as TastyBurgerButton from 'vue-tasty-burgers';
import 'vue-tasty-burgers/dist/vue-tasty-burgers.css';
// import VModal from 'vue-js-modal';
import VueFriendlyIframe from 'vue-friendly-iframe';
import AOS from "aos";
import "aos/dist/aos.css";
import VueAnalytics from 'vue-analytics';
import VueMeta from 'vue-meta'
import axios from 'axios'
import VueAxios from 'vue-axios'
// import VuePaginate from 'vue-paginate'


// Vue.use(VuePaginate)
Vue.use(VueAxios, axios)
Vue.use(VueMeta)

var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);


// boolean
Vue.use(VueAnalytics, {
    id: 'UA-103274080-10'
})
 
// Vue.use(VModal)

Vue.use(TastyBurgerButton);

Vue.config.productionTip = false;

Vue.component('vue-friendly-iframe', VueFriendlyIframe);

// Vue.use(VueAnalytics, {
//   id: 'UA-103274080-10',
//   checkDuplicatedScript: true,
//   disableScriptLoader: true,
//   router
// })

new Vue({
	  mounted() {
    AOS.init({ disable: "phone" });
    // console.log(this.$router)
    // console.log(this.$route)
  },
  router,
  store,
  render: h => h(App)
}).$mount('#app')
