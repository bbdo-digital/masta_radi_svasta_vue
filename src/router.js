import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import VueRouterSitemap      from 'vue-router-sitemap';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/kako-sudjelovati',
      name: 'kako-sudjelovati',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/KakoSudjelovati.vue')
    },
    {
      path: '/glasaj-za-dobrobit-djece',
      name: 'glasaj-za-dobrobit-djece',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/GoodCause.vue')
    },
    {
      path: '/odaberi-humanitarnu-udrugu',
      name: 'odaberi-humanitarnu-udrugu',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/GoodCause.vue')
    },
    {
      path: '/glasaj-za-omiljeni-crtez',
      name: 'glasaj-za-omiljeni-crtez',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/GlasujZaSvogFavorita.vue')
    },
    {
      path: '/raspored-crtanja',
      name: 'raspored-crtanja',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/RasporedCrtanja.vue')
    },
    {
      path: '/nagrade',
      name: 'nagrade',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Nagrade.vue')
    },
    {

      path: '/objava-dobitnika',
      name: 'objava-dobitnika',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/ObjavaDobitnika.vue')
       },
      {
      path: '/pravila',
      name: 'pravila',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Pravila.vue')

    },
    {
      path: '/pravila-za-dobrobit-djece',
      name: 'pravila',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Pravila.vue')

    },
     {
      path: '/o-sagoskatt-kolekciji',
      name: 'o-sagoskatt-kolekciji',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/SagoskatZivotinje.vue')

    },
    
    { path: "*", 
      name:'404',
    component: () => import(/* webpackChunkName: "about" */ './views/404.vue')
  },
  { path: '*', redirect: '/404' },

  //   { path: '/', component: 'home' },  
  // { path: '*', redirect: '/' }, 
  ]
})

//console.log($router.options.route)
// export const sitemapMiddleware = () => {
//   return (req, res) => {
//     res.set('Content-Type', 'application/xml');

//     const staticSitemap = path.resolve('dist/static', 'sitemap.xml');
//     const filterConfig = {
//       isValid: false,
//       rules: [
//         /\/example-page/,
//         /\*/,
//       ],
//     };

//     new VueRouterSitemap(router).filterPaths(filterConfig).build('http://localhost:8080/').save(staticSitemap);

//     return res.sendFile(staticSitemap);
//   };
// };

// app.get('/sitemap.xml', sitemapMiddleware());

// function getRoutesXML() {
//   const list = getRoutesList(router.options.routes, 'http://localhost:8080')
//     .map(route => `<url><loc>${route}</loc></url>`)
//     .join('\r\n');
//   return `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
//     ${list}
//   </urlset>`;
// }

// getRoutesXML();


